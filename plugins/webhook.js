'use strict'

var express = require("express"),
    app = express(),
    bodyParser = require("body-parser"),
    xhub = require("express-x-hub"),
    fs = require('fs'),
    dir = './tmp',
    exec = require('child_process').exec,
    child,
    git = require('./plugins/git.js'),
    pandoc = require('./plugins/transform.js');
    exports = module.exports = {}




exports.getWebHook = function(dir) {
  app.post("/webhook", function (req, res) {

    var command = req.headers

    switch(command['x-github-event']) {
      case "push": // on push event
        res.send("Event commit trigger")
        console.log("Push event received")
        fs.exists(dir, function(exist) { // check if tmp folder already exist
          if(!exist) {
            fs.mkdirSync(dir); // if tmp folder don't exist, create it
            console.log('tmp folder created')
            exec('git clone git@github.com:derniercri/playbook.git ' + dir, function(err, stdout, stderr) {
              console.log('clone status : ', stderr);
              console.log('repo cloned successfully');
              pandoc.sendFileToPandoc();
            });
          }
          else {
            exec('rm -rf ./tmp', function(stdout,stderr) { // delete tmp folder
              exec('git clone git@github.com:derniercri/playbook.git ' + dir, function(err, stdout, stderr) {
                console.log('clone status : ', stderr);
                console.log('repo cloned successfully');
                console.log('tmp folder cleaned')
                pandoc.sendFileToPandoc();
              });
            });
          }
        });
      break
      default:
      res.status(400).send("Event not supported : " + command)
      console.log("Event not supported : " + req.headers["x-github-event"])
    }
  })
}