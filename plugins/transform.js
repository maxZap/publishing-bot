'use strict'

var fs = require('fs'),
dir = './tmp',
exec = require('child_process').exec,
child,
exports = module.exports = {},
splittedFiles,



bucket = require("./s3.js");

 exports.sendFileToPandoc = function() {
  fs.readdirSync(dir).forEach(function(file) {
    splittedFiles = file.split('.'); //split file to keep only the name without extension
    if(file.length == 0 || file[0] == "" ) { //check if folder is not empty
      console.log('Error, folder ' + dir + 'is empty');
    }
    else if(file != ".git" && splittedFiles[1] == "md" || splttedFiles[1] == "adoc") {
      exec('pandoc '+ file +  ' -o ' + splittedFiles[0] + '.pdf --template=default.latex',{cwd:'./tmp'}, function(stdout,stderr,err) { //put files in pandoc for converts
        console.log('pdf file created successfully')
        exec('mv ./tmp/*.pdf ./pdf', function(stdout, stderr) {
          console.log('pdf files moved to pdf directory');
          bucket.connectBucket()
        });
      });
    }
  });
}